use std::{collections::HashMap, fs, process::exit};

fn atoi(input: String) -> u32 {
    let mut number = 0;
    let true_val = convert_str_numbers_to_ints(input);
    for character in true_val.chars() {
        let value: i32 = match character.to_digit(10) {
            Some(number) => number.try_into().unwrap(),
            None => -1,
        };

        if value >= 0 {
            number = number * 10 + value;
        }
    }

    return number.try_into().unwrap();
}

fn convert_str_numbers_to_ints(input: String) -> String {
    let mut new_input = input;

    let number_strings = HashMap::from([
        (9, "nine"),
        (8, "eight"),
        (3, "three"),
        (1, "one"),
        (4, "four"),
        (5, "five"),
        (6, "six"),
        (7, "seven"),
        (0, "zero"),
        (2, "two"),
    ]);

    // Eightwone -> eight8eightwoone -> eight8eightwo2twone -> eight8eightwo2twone1one
    for (int_value, string_value) in number_strings {
        new_input = new_input.replace(
            string_value,
            (string_value.to_owned()
                + int_value.to_string().as_str()
                + string_value.to_owned().as_str())
            .to_string()
            .as_str(),
        );
    }
    return new_input;
}
fn convert_file_content_to_numbers(file_path: String) -> Vec<u32> {
    let mut numbers: Vec<u32> = Vec::new();

    let content: Vec<String> = fs::read_to_string(file_path)
        .expect("Error reading the file.")
        .lines()
        .map(String::from)
        .collect();

    for line in content.iter() {
        numbers.push(atoi(line.to_string()));
    }

    return numbers;
}

fn day1() {
    let numbers = convert_file_content_to_numbers("input.txt".to_string());
    let mut sum: u32 = 0;

    for num in numbers {
        let last_digit: u32 = num % 10;

        let mut first_digit: u32 = num;

        while first_digit > 9 {
            first_digit /= 10;
        }
        let number = first_digit * 10 + last_digit;
        sum += number;
    }

    println!("{}", sum);
}

fn day2() {
    let input_file = "./input-day2.txt";
    let (r, g, b) = (12, 13, 14);

    let file_content = fs::read_to_string(input_file).unwrap();
    let mut sum: u32 = 0;

    for line in file_content.split("\n") {
        let mut set_okay: bool = true;
        let first_part = line.split(": ").into_iter().next().unwrap();
        let continued_line = line.split(": ").into_iter().last().unwrap().trim();
        let game_number: u32 = atoi(first_part.to_string());
        let sets = continued_line.split("; ");

        for set in sets.into_iter() {
            let options = set.split(", ");
            for option in options.into_iter() {
                let mut option_iter = option.split_whitespace().into_iter();
                let number = option_iter.next().unwrap();
                let color = option_iter.next().unwrap();

                if color.to_string() == "red" {
                    if atoi(number.to_string()) > r {
                        set_okay = false;
                    }
                } else if color.to_string() == "green" {
                    if atoi(number.to_string()) > g {
                        set_okay = false;
                    }
                } else if color.to_string() == "blue" {
                    if atoi(number.to_string()) > b {
                        set_okay = false;
                    }
                }
            }
        }
        if set_okay {
            sum += game_number;
        }
    }

    println!("{}", sum);
}

fn main() {
    // day1();
    day2();
}

#[test]
fn test_atoi() {
    assert_eq!(atoi("a".to_string()), 0);
    assert_eq!(atoi("12".to_string()), 12);
    assert_eq!(atoi("12a".to_string()), 12);
    assert_eq!(atoi("eightwothree".to_string()), 83);
    assert_eq!(atoi("7pqrstsixteen".to_string()), 76);
}
